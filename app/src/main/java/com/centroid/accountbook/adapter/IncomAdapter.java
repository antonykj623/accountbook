package com.centroid.accountbook.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.accountbook.R;
import com.centroid.accountbook.data.DatabaseClient;
import com.centroid.accountbook.domain.Income;
import com.centroid.accountbook.views.AddIncomeActivity;

import java.util.List;

public class IncomAdapter extends RecyclerView.Adapter<IncomAdapter.IncomHolder> {

    Context context;
    List<Income>incomes;
    IncomeDataAdapter incomeDataAdapter;

    public IncomAdapter(Context context, List<Income> incomes,IncomeDataAdapter incomeDataAdapter) {
        this.context = context;
        this.incomes = incomes;
        this.incomeDataAdapter=incomeDataAdapter;
    }

    public class IncomHolder extends RecyclerView.ViewHolder {

        TextView txtAmount,txtdate,txtSource;
        ImageView imgedit,imgdelete;


        public IncomHolder(@NonNull View itemView) {
            super(itemView);
            txtSource=itemView.findViewById(R.id.txtSource);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtAmount=itemView.findViewById(R.id.txtAmount);

            imgedit=itemView.findViewById(R.id.imgedit);
            imgdelete=itemView.findViewById(R.id.imgdelete);

            imgedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, AddIncomeActivity.class);
                    intent.putExtra("Data",incomes.get(getAdapterPosition()));

                    context.startActivity(intent);

                }
            });

            imgdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new DeleteDataFromAsync(context,getAdapterPosition()).execute("");

                }
            });
        }
    }


    @NonNull
    @Override
    public IncomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_incomedata,parent,false);



        return new IncomHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IncomHolder holder, int position) {

        holder.txtAmount.setText("Amount : "+incomes.get(position).getIncomeAmount()+" "+context.getString(R.string.rs));
        holder.txtdate.setText("Date : "+incomes.get(position).getIncomedate());
        holder.txtSource.setText("Source : "+incomes.get(position).getIncomeSource());

    }

    @Override
    public int getItemCount() {
        return incomes.size();
    }


    public class DeleteDataFromAsync extends AsyncTask<String,String,String>
    {

        Context context;
        int pos;

        public DeleteDataFromAsync(Context context, int pos) {
            this.context = context;
            this.pos = pos;
        }

        @Override
        protected String doInBackground(String... strings) {

            DatabaseClient.getInstance(context).getAppDatabase().incomeDao().delete(incomes.get(pos));
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            incomes.remove(pos);

            notifyItemRemoved(pos);

            incomeDataAdapter.notifyDataSetChanged();
        }
    }

    }
