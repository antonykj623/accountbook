package com.centroid.accountbook.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.centroid.accountbook.fragments.ExpenseFragment;
import com.centroid.accountbook.fragments.IncomeFragment;
import com.centroid.accountbook.fragments.TotalFragment;

import java.util.List;

public class HomepagerAdapter extends FragmentPagerAdapter {
    List<Fragment>fragments;

    public HomepagerAdapter(@NonNull FragmentManager fm, List<Fragment>fragments) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fragments=fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {




        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
