package com.centroid.accountbook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.accountbook.R;
import com.centroid.accountbook.domain.TotalBudget;
import com.centroid.accountbook.fragments.TotalFragment;

import java.util.List;

public class TotalBudgetAdapter extends RecyclerView.Adapter<TotalBudgetAdapter.TotalBudgetHolder> {


    Context context;
    List<TotalBudget>totalBudgets;
    TotalFragment totalFragment;
    String arr[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};


    public TotalBudgetAdapter(Context context, List<TotalBudget> totalBudgets, TotalFragment totalFragment) {
        this.context = context;
        this.totalBudgets = totalBudgets;
        this.totalFragment=totalFragment;
    }

    public class TotalBudgetHolder extends RecyclerView.ViewHolder{

        TextView txtAmount;

        ImageView imgdownload;


        public TotalBudgetHolder(@NonNull View itemView) {
            super(itemView);
            txtAmount=itemView.findViewById(R.id.txtAmount);
            imgdownload=itemView.findViewById(R.id.imgdownload);


            imgdownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

totalFragment.downloadBudgetFile(totalBudgets.get(getAdapterPosition()));

                }
            });
        }
    }


    @NonNull
    @Override
    public TotalBudgetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_total,parent,false);

        return new TotalBudgetHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TotalBudgetHolder holder, int position) {

        String a[]=totalBudgets.get(position).getMonthyear().split("-");


        int m=Integer.parseInt(a[0]);

        String month=arr[m-1];

        holder.txtAmount.setText(""+month+"-"+a[1]+"\nTotal Income  : "+totalBudgets.get(position).getTotalIncome()+context.getString(R.string.rs)+"\nTotal Expense : "+totalBudgets.get(position).getTotalExpense()+context.getString(R.string.rs));

    }

    @Override
    public int getItemCount() {
        return totalBudgets.size();
    }
}
