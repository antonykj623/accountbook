package com.centroid.accountbook.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.accountbook.R;
import com.centroid.accountbook.data.DatabaseClient;
import com.centroid.accountbook.domain.Expense;
import com.centroid.accountbook.views.AddExpenseActivity;
import com.centroid.accountbook.views.AddIncomeActivity;

import java.util.List;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ExpenseHolder> {


    Context context;
    List<Expense>expenses;

    ExpenseDataAdapter expenseDataAdapter;

    public ExpenseAdapter(Context context, List<Expense> expenses, ExpenseDataAdapter expenseDataAdapter) {
        this.context = context;
        this.expenses = expenses;
        this.expenseDataAdapter = expenseDataAdapter;
    }

    public class ExpenseHolder extends RecyclerView.ViewHolder{

        TextView txtAmount,txtdate,txtSource;
        ImageView imgedit,imgdelete,img;

        public ExpenseHolder(@NonNull View itemView) {
            super(itemView);

            txtSource=itemView.findViewById(R.id.txtSource);
            txtdate=itemView.findViewById(R.id.txtdate);
            txtAmount=itemView.findViewById(R.id.txtAmount);

            imgedit=itemView.findViewById(R.id.imgedit);
            imgdelete=itemView.findViewById(R.id.imgdelete);
            img=itemView.findViewById(R.id.img);

            imgedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Intent intent=new Intent(context, AddExpenseActivity.class);
                    intent.putExtra("Data",expenses.get(getAdapterPosition()));

                    context.startActivity(intent);
                }
            });

            imgdelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    new DeleteDataFromAsync(context,getAdapterPosition()).execute("");
                }
            });
        }
    }

    @NonNull
    @Override
    public ExpenseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_incomedata,parent,false);



        return new ExpenseHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenseHolder holder, int position) {

        holder.txtAmount.setText("Amount : "+expenses.get(position).getExpenseAmount()+" "+context.getString(R.string.rs));
        holder.txtdate.setText("Date : "+expenses.get(position).getExpensedate());
        holder.txtSource.setText("Purpose : "+expenses.get(position).getExpenseSource());
        holder.img.setImageResource(R.drawable.ic_accounting);


    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }


    public class DeleteDataFromAsync extends AsyncTask<String,String,String>
    {

        Context context;
        int pos;

        public DeleteDataFromAsync(Context context, int pos) {
            this.context = context;
            this.pos = pos;
        }

        @Override
        protected String doInBackground(String... strings) {

            DatabaseClient.getInstance(context).getAppDatabase().expenseDao().delete(expenses.get(pos));
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            expenses.remove(pos);

            notifyItemRemoved(pos);

            expenseDataAdapter.notifyDataSetChanged();
        }
    }

}
