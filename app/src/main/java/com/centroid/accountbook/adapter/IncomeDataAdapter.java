package com.centroid.accountbook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.accountbook.R;
import com.centroid.accountbook.domain.Income;
import com.centroid.accountbook.domain.IncomeTypeList;

import java.util.List;

public class IncomeDataAdapter extends RecyclerView.Adapter<IncomeDataAdapter.IncomeHolder> {

    Context context;
    List<IncomeTypeList>incomeTypeLists;


    String arr[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    public IncomeDataAdapter(Context context, List<IncomeTypeList> incomes) {
        this.context = context;
        this.incomeTypeLists = incomes;
    }

    public class IncomeHolder extends RecyclerView.ViewHolder{

        TextView txtAmount;

        RecyclerView recycler;

        CardView cardWishlist;

        public IncomeHolder(@NonNull View itemView) {
            super(itemView);


            txtAmount=itemView.findViewById(R.id.txtAmount);
            recycler=itemView.findViewById(R.id.recycler);
            cardWishlist=itemView.findViewById(R.id.cardWishlist);

            cardWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(incomeTypeLists.get(getAdapterPosition()).getSelected()==0)
                    {

                        for (IncomeTypeList incomeTypeList:incomeTypeLists
                             ) {

                            incomeTypeList.setSelected(0);

                        }

                        incomeTypeLists.get(getAdapterPosition()).setSelected(1);
                    }
                    else {


                        for (IncomeTypeList incomeTypeList:incomeTypeLists
                        ) {

                            incomeTypeList.setSelected(0);

                        }

                        incomeTypeLists.get(getAdapterPosition()).setSelected(0);


                    }


                    notifyDataSetChanged();


                }


            });
        }
    }

    @NonNull
    @Override
    public IncomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_monthyear,parent,false);

        return new IncomeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IncomeHolder holder, int position) {




        double totalamount=0;

        for (int i = 0; i < incomeTypeLists.get(position).getIncomes().size(); i++) {

totalamount=totalamount+Double.parseDouble(incomeTypeLists.get(position).getIncomes().get(i).getIncomeAmount());

        }

        String a[]=incomeTypeLists.get(position).getMonthyear().split("-");


        int m=Integer.parseInt(a[0]);

        String month=arr[m-1];



        holder.txtAmount.setText(month+"-"+a[1]+"\n"+totalamount+context.getString(R.string.rs));


        if(incomeTypeLists.get(position).getSelected()==0)
        {
            holder.recycler.setVisibility(View.GONE);
        }
        else {

            holder.recycler.setVisibility(View.VISIBLE);
                   }

        holder.recycler.setLayoutManager(new LinearLayoutManager(context));
        holder.recycler.setAdapter(new IncomAdapter(context,incomeTypeLists.get(position).getIncomes(),IncomeDataAdapter.this));


    }

    @Override
    public int getItemCount() {
        return incomeTypeLists.size();
    }
}
