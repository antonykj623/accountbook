package com.centroid.accountbook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.accountbook.R;
import com.centroid.accountbook.domain.Expense;
import com.centroid.accountbook.domain.ExpenseTypeList;
import com.centroid.accountbook.domain.IncomeTypeList;

import java.util.List;

public class ExpenseDataAdapter extends RecyclerView.Adapter<ExpenseDataAdapter.ExpenseHolder> {


    Context context;
    List<ExpenseTypeList>expenseTypeLists;

    String arr[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    public ExpenseDataAdapter(Context context, List<ExpenseTypeList>expenseTypeLists) {
        this.context = context;
        this.expenseTypeLists = expenseTypeLists;
    }

    public class ExpenseHolder extends RecyclerView.ViewHolder
    {

        TextView txtAmount;

        RecyclerView recycler;

        CardView cardWishlist;

        public ExpenseHolder(@NonNull View itemView) {
            super(itemView);


            txtAmount=itemView.findViewById(R.id.txtAmount);
            recycler=itemView.findViewById(R.id.recycler);
            cardWishlist=itemView.findViewById(R.id.cardWishlist);

            cardWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if(expenseTypeLists.get(getAdapterPosition()).getSelected()==0)
                    {

                        for (ExpenseTypeList incomeTypeList:expenseTypeLists
                        ) {

                            incomeTypeList.setSelected(0);

                        }

                        expenseTypeLists.get(getAdapterPosition()).setSelected(1);
                    }
                    else {


                        for (ExpenseTypeList incomeTypeList:expenseTypeLists
                        ) {

                            incomeTypeList.setSelected(0);

                        }

                        expenseTypeLists.get(getAdapterPosition()).setSelected(0);


                    }


                    notifyDataSetChanged();


                }


            });


        }
    }

    @NonNull
    @Override
    public ExpenseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_monthyear,parent,false);



        return new ExpenseHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenseHolder holder, int position) {

        double totalamount=0;

        for (int i = 0; i < expenseTypeLists.get(position).getExpenses().size(); i++) {

            totalamount=totalamount+Double.parseDouble(expenseTypeLists.get(position).getExpenses().get(i).getExpenseAmount());

        }

        String a[]=expenseTypeLists.get(position).getMonthyear().split("-");


        int m=Integer.parseInt(a[0]);

        String month=arr[m-1];



        holder.txtAmount.setText(month+"-"+a[1]+"\n"+totalamount+context.getString(R.string.rs));


        if(expenseTypeLists.get(position).getSelected()==0)
        {
            holder.recycler.setVisibility(View.GONE);
        }
        else {

            holder.recycler.setVisibility(View.VISIBLE);
        }
//
        holder.recycler.setLayoutManager(new LinearLayoutManager(context));
        holder.recycler.setAdapter(new ExpenseAdapter(context,expenseTypeLists.get(position).getExpenses(),ExpenseDataAdapter.this));


    }

    @Override
    public int getItemCount() {
        return expenseTypeLists.size();
    }
}
