package com.centroid.accountbook.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.centroid.accountbook.domain.Expense;

import java.util.List;

@Dao
public interface ExpenseDao {

    @Query("SELECT * FROM Expense")
    List<Expense> getAll();

    @Insert
    void insert(Expense task);

    @Delete
    void delete(Expense task);

    @Update
    void update(Expense task);
}
