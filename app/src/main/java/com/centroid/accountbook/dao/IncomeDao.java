package com.centroid.accountbook.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.centroid.accountbook.domain.Income;

import java.util.List;

@Dao
public interface IncomeDao {


    @Query("SELECT * FROM income")
    List<Income> getAll();

    @Insert
    void insert(Income task);

    @Delete
    void delete(Income task);

    @Update
    void update(Income task);
}
