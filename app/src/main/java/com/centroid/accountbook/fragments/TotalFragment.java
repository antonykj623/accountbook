package com.centroid.accountbook.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.centroid.accountbook.R;
import com.centroid.accountbook.adapter.ExpenseDataAdapter;
import com.centroid.accountbook.adapter.IncomeDataAdapter;
import com.centroid.accountbook.adapter.TotalBudgetAdapter;
import com.centroid.accountbook.data.DatabaseClient;
import com.centroid.accountbook.domain.Expense;
import com.centroid.accountbook.domain.Income;
import com.centroid.accountbook.domain.TotalBudget;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class TotalFragment extends Fragment {


    public TotalFragment() {
        // Required empty public constructor
    }

    View v;

    RecyclerView recycler;

    List<Expense>expenses;
    List<Income>incomes;

    Set<String>incomedates=new HashSet<>();

    List<TotalBudget>totalBudgets;

    TotalBudget currenttotalBudget=null;

    String arr[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_total, container, false);
        recycler=v.findViewById(R.id.recycler);
        expenses=new ArrayList<>();
        incomes=new ArrayList<>();
        totalBudgets=new ArrayList<>();


        getAllExpense();


        return v;
    }


    public void getAllExpense()
    {
        expenses=new ArrayList<>();
        incomes=new ArrayList<>();
        totalBudgets=new ArrayList<>();

        new GetExpenseData().execute("");
    }


    public class GetExpenseData extends AsyncTask<String,String,List<Expense>>
    {

        @Override
        protected List<Expense> doInBackground(String... strings) {



            List<Expense> expenses= DatabaseClient.getInstance(getActivity()).getAppDatabase().expenseDao().getAll();



            if(expenses.size()>0)
            {


                Collections.reverse(expenses);




            }



            return expenses;
        }


        @Override
        protected void onPostExecute(List<Expense> s) {
            super.onPostExecute(s);

            expenses.addAll(s);

new GetIncomeDataFromAsync().execute("");
        }
    }





    public class GetIncomeDataFromAsync extends AsyncTask<String,String,List<Income>>
    {



        @Override
        protected List<Income> doInBackground(String... strings) {





            List<Income>incomes= DatabaseClient.getInstance(getActivity()).getAppDatabase().incomeDao().getAll();


            if(incomes.size()>0) {

                Collections.reverse(incomes);



            }
            return incomes;
        }

        @Override
        protected void onPostExecute(List<Income> s) {
            super.onPostExecute(s);
            incomes.addAll(s);

            collectDate();


        }
    }


    public void collectDate()
    {

        incomedates.clear();
        totalBudgets.clear();

        for (Income income:incomes
             ) {

            String d=income.getIncomeMonth()+"-"+income.getIncomeyear();

            incomedates.add(d);
        }



        checkIncomeBudget();



    }

    public void checkIncomeBudget()
    {

        for (String date:incomedates)
        {
            TotalBudget totalBudget=new TotalBudget();
            totalBudget.setMonthyear(date);

            double totalincome=0;
            double totalexpense=0;


            for (Income income:incomes
            ) {

                String d = income.getIncomeMonth() + "-" + income.getIncomeyear();

                if(d.equals(date))
                {

                    double t=Double.parseDouble(income.getIncomeAmount());


                    totalincome=totalincome+t;

                }


            }

            totalBudget.setTotalIncome(totalincome);



            for (Expense expense:expenses)
            {

                String d = expense.getExpenseMonth() + "-" + expense.getExpenseyear();

                if(d.equals(date))
                {
                    double t=Double.parseDouble(expense.getExpenseAmount());
                    totalexpense=totalexpense+t;


                }


            }

            totalBudget.setTotalExpense(totalexpense);

            totalBudgets.add(totalBudget);



        }


        if(totalBudgets.size()>0)
        {

            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycler.setAdapter(new TotalBudgetAdapter(getActivity(),totalBudgets,TotalFragment.this));



        }

    }


    public void downloadBudgetFile(TotalBudget totalBudget)
    {

        currenttotalBudget=totalBudget;




checkPermission();






    }


    private void checkPermission()
    {

        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {

requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},111);

        }
        else {

            prepareData(currenttotalBudget);
        }
    }





    private void prepareData(TotalBudget totalBudget)
    {

try {


    String a[]=totalBudget.getMonthyear().split("-");


    int m=Integer.parseInt(a[0]);

    String month=arr[m-1];







    String path = getActivity().getExternalCacheDir() + "/" + totalBudget.getMonthyear();

    File folderpath = new File(path);
    if (!folderpath.exists()) {
        folderpath.mkdir();
    }



    String filepath = folderpath.getAbsolutePath() + "/" + totalBudget.getMonthyear() + ".pdf";


    File f = new File(filepath);

    if (!f.exists()) {
        f.createNewFile();
    }
    else {

        f.delete();
        f.createNewFile();
    }


    PdfDocument pdfDocument = new PdfDocument(new PdfWriter(f.getAbsolutePath()));

    Document layoutDocument = new Document(pdfDocument);

    layoutDocument.add(new Paragraph("BUDGET DETAILS\n\n").setBold().setUnderline().setTextAlignment(TextAlignment.CENTER));



    layoutDocument.add(new Paragraph("INCOME LIST").setTextAlignment(TextAlignment.LEFT).setMultipliedLeading(0.2f));
    layoutDocument.add(new Paragraph("TOTAL AMOUNT : "+currenttotalBudget.getTotalIncome()+" Rs").setMultipliedLeading(.2f));
    layoutDocument.add(new Paragraph(""+month+"-"+a[1]).setMultipliedLeading(.2f));


    Table table1 = new Table(UnitValue.createPointArray(new float[]{60f, 120f, 50f, 80f, 170f}));

    table1.addCell(new Paragraph("S.N.O.").setBold());
    table1.addCell(new Paragraph("AMOUNT").setBold());
    table1.addCell(new Paragraph("DATE").setBold());
    table1.addCell(new Paragraph("MONTH & YEAR").setBold());
    table1.addCell(new Paragraph("SOURCE").setBold());

 int j=0;


    for (int i=0;i<incomes.size();i++) {

        Income income=incomes.get(i);

        String d = income.getIncomeMonth() + "-" + income.getIncomeyear();

        if (d.equals(totalBudget.getMonthyear())) {

            j++;

            table1.addCell(new Paragraph((j)+""));
            table1.addCell(new Paragraph(income.getIncomeAmount()+" Rs"));
            table1.addCell(new Paragraph(income.getIncomedate()+""));
            table1.addCell(new Paragraph(month+"-"+a[1]));
            table1.addCell(new Paragraph(income.getIncomeSource()));
        }

    }



    layoutDocument.add(table1);























    layoutDocument.add(new Paragraph("\n\n\nEXPENSE LIST").setTextAlignment(TextAlignment.LEFT).setMultipliedLeading(0.2f));
    layoutDocument.add(new Paragraph("TOTAL AMOUNT : "+currenttotalBudget.getTotalExpense()+" Rs").setMultipliedLeading(.2f));
    layoutDocument.add(new Paragraph(""+month+"-"+a[1]).setMultipliedLeading(.2f));


    Table table = new Table(UnitValue.createPointArray(new float[]{60f, 120f, 50f, 80f, 170f}));

    table.addCell(new Paragraph("S.N.O.").setBold());
    table.addCell(new Paragraph("AMOUNT").setBold());
    table.addCell(new Paragraph("DATE").setBold());
    table.addCell(new Paragraph("MONTH & YEAR").setBold());
    table.addCell(new Paragraph("PURPOSE").setBold());





    int k=0;

    for (int i=0;i<expenses.size();i++) {

        Expense expense=expenses.get(i);

        String d = expense.getExpenseMonth() + "-" + expense.getExpenseyear();

        if (d.equals(totalBudget.getMonthyear())) {

            k++;



            table.addCell(new Paragraph((k)+""));
            table.addCell(new Paragraph(expense.getExpenseAmount()+" Rs"));
            table.addCell(new Paragraph(expense.getExpensedate()+""));
            table.addCell(new Paragraph(month+"-"+a[1]+""));
            table.addCell(new Paragraph(expense.getExpenseSource()));


        }
    }



    layoutDocument.add(table);


    double balance=currenttotalBudget.getTotalIncome()-currenttotalBudget.getTotalExpense();


    layoutDocument.add(new Paragraph("\n\nBALANCE : "+balance+" Rs").setBold().setUnderline().setTextAlignment(TextAlignment.CENTER));

    layoutDocument.add(new Paragraph("SHOULD MAINTAIN BUDGET PROPERLY ").setBold().setUnderline().setTextAlignment(TextAlignment.CENTER));


    layoutDocument.close();


    /*
    *
    *
    *
    *
    *
    *
    * */


    Toast.makeText(getActivity(),"File downloaded successfully",Toast.LENGTH_SHORT).show();

    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
    {

        Uri photoURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", f);

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(photoURI, "application/pdf");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        getActivity().startActivity(intent);

    }
    else {

        Intent intent =new  Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(f), "application/pdf");
        startActivity(intent);
    }







}catch (Exception e)
{
    Toast.makeText(getActivity(),"Error while downloading file",Toast.LENGTH_SHORT).show();
}
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==111)
        {
            checkPermission();
        }
    }
}
