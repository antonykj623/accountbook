package com.centroid.accountbook.fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.centroid.accountbook.R;
import com.centroid.accountbook.adapter.IncomeDataAdapter;
import com.centroid.accountbook.data.DatabaseClient;
import com.centroid.accountbook.domain.Income;
import com.centroid.accountbook.domain.IncomeTypeList;
import com.centroid.accountbook.views.AddIncomeActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncomeFragment extends Fragment {


    public IncomeFragment() {
        // Required empty public constructor
    }

    View v;

    FloatingActionButton fab;

    RecyclerView recycler;



    List<IncomeTypeList>incomeTypeLists;

    String date="";

    Set<String>dateSet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_income, container, false);
        fab=v.findViewById(R.id.fab);
        recycler=v.findViewById(R.id.recycler);





        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), AddIncomeActivity.class));
            }
        });


        getIncomeData();
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void getIncomeData()
    {

//        Thread thread=new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//
//            }
//        });
//
//        thread.start();


        new GetDataFromAsync().execute("");
    }


    private void classifyIncome(List<Income>incomes,String date)
    {

        List<Income> classifiedincome = new ArrayList<>();
        for (int j=0;j<incomes.size();j++) {

            String d = incomes.get(j).getIncomeMonth() + "-" + incomes.get(j).getIncomeyear();


            if (d.equals(date)) {
                classifiedincome.add(incomes.get(j));

            }



        }



        IncomeTypeList incomeTypeList=new IncomeTypeList();
        incomeTypeList.setMonthyear(date);
        incomeTypeList.setIncomes(classifiedincome);
        incomeTypeList.setSelected(0);
        incomeTypeLists.add(incomeTypeList);

    }

    public class GetDataFromAsync extends AsyncTask<String,String,String>
    {



        @Override
        protected String doInBackground(String... strings) {

            incomeTypeLists=new ArrayList<>();



            List<Income>incomes= DatabaseClient.getInstance(getActivity()).getAppDatabase().incomeDao().getAll();


            if(incomes.size()>0) {

                Collections.reverse(incomes);


                dateSet = new HashSet<>();

                for (int i = 0; i < incomes.size(); i++) {
                    String d = incomes.get(i).getIncomeMonth() + "-" + incomes.get(i).getIncomeyear();

                    dateSet.add(d);


                }


                for (int j = 0; j < dateSet.size(); j++) {

                    String d[] = dateSet.toArray(new String[dateSet.size()]);

                    String date = d[j];

                    classifyIncome(incomes, date);


                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycler.setAdapter(new IncomeDataAdapter(getActivity(), incomeTypeLists));
        }
    }

}
