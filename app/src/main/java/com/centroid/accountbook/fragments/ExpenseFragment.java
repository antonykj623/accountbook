package com.centroid.accountbook.fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.centroid.accountbook.R;
import com.centroid.accountbook.adapter.ExpenseDataAdapter;
import com.centroid.accountbook.adapter.IncomeDataAdapter;
import com.centroid.accountbook.data.DatabaseClient;
import com.centroid.accountbook.domain.Expense;
import com.centroid.accountbook.domain.ExpenseTypeList;
import com.centroid.accountbook.domain.Income;
import com.centroid.accountbook.domain.IncomeTypeList;
import com.centroid.accountbook.views.AddExpenseActivity;
import com.centroid.accountbook.views.AddIncomeActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpenseFragment extends Fragment {


    public ExpenseFragment() {
        // Required empty public constructor
    }

    FloatingActionButton fab;

    View v;

    RecyclerView recycler;

    List<ExpenseTypeList>expenseTypeLists;

    Set<String> dateSet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_expense, container, false);

        fab=v.findViewById(R.id.fab);
        recycler=v.findViewById(R.id.recycler);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(getActivity(), AddExpenseActivity.class));
            }
        });

        getExpense();


        return v;
    }





    public void getExpense()
    {
        new GetExpenseData().execute("");
    }




    public class GetExpenseData extends AsyncTask<String,String,String>
    {

        @Override
        protected String doInBackground(String... strings) {

            expenseTypeLists=new ArrayList<>();

            List<Expense> expenses= DatabaseClient.getInstance(getActivity()).getAppDatabase().expenseDao().getAll();



            if(expenses.size()>0)
            {
                dateSet=new HashSet<>();

                Collections.reverse(expenses);

                for (int i = 0; i < expenses.size(); i++) {
                    String d = expenses.get(i).getExpenseMonth() + "-" + expenses.get(i).getExpenseyear();

                    dateSet.add(d);


                }

                for (int j = 0; j < dateSet.size(); j++) {

                    String d[] = dateSet.toArray(new String[dateSet.size()]);

                    String date = d[j];

                    classifyExpense(expenses, date);


                }
            }



            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            recycler.setAdapter(new ExpenseDataAdapter(getActivity(), expenseTypeLists));
        }
    }


    private void classifyExpense(List<Expense>incomes,String date)
    {

        List<Expense> classifiedincome = new ArrayList<>();
        for (int j=0;j<incomes.size();j++) {

            String d = incomes.get(j).getExpenseMonth() + "-" + incomes.get(j).getExpenseyear();


            if (d.equals(date)) {
                classifiedincome.add(incomes.get(j));

            }



        }



        ExpenseTypeList incomeTypeList=new ExpenseTypeList();
        incomeTypeList.setMonthyear(date);
        incomeTypeList.setExpenses(classifiedincome);
        incomeTypeList.setSelected(0);
        expenseTypeLists.add(incomeTypeList);

    }

}
