package com.centroid.accountbook.data;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.centroid.accountbook.dao.ExpenseDao;
import com.centroid.accountbook.dao.IncomeDao;
import com.centroid.accountbook.domain.Expense;
import com.centroid.accountbook.domain.Income;


@Database(entities = {Income.class, Expense.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {

   public abstract IncomeDao incomeDao();

   public abstract ExpenseDao expenseDao();
}
