package com.centroid.accountbook.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.accountbook.R;
import com.centroid.accountbook.data.DatabaseClient;
import com.centroid.accountbook.domain.Expense;
import com.centroid.accountbook.domain.Income;

import java.util.Calendar;

public class AddExpenseActivity extends AppCompatActivity {

    ImageView imgback,imgdate;

    TextView txtDate;

    EditText input_amount,input_source;

    Button btn_submit;

    private int mYear, mMonth, mDay;

    String selectedmonth="",selectedYear="",date="";

    Thread t=null;
    
    Expense expense_data=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);
        getSupportActionBar().hide();

        expense_data=(Expense) getIntent().getSerializableExtra("Data");

        imgback=findViewById(R.id.imgback);
        btn_submit=findViewById(R.id.btn_submit);

        imgdate=findViewById(R.id.imgdate);
        txtDate=findViewById(R.id.txtDate);
        input_amount=findViewById(R.id.input_amount);
        input_source=findViewById(R.id.input_source);
        btn_submit=findViewById(R.id.btn_submit);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();

            }
        });

        imgdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!date.equalsIgnoreCase(""))
                {
                    if(!input_amount.getText().toString().equalsIgnoreCase(""))
                    {

                        if(expense_data!=null)
                        {

                            Expense income = new Expense();
                            income.setExpense_id(expense_data.getExpense_id());
                            income.setExpenseAmount(input_amount.getText().toString());
                            income.setExpensedate(date);
                            income.setExpenseMonth(selectedmonth);
                            income.setExpenseSource(input_source.getText().toString());
                            income.setExpenseyear(selectedYear);
                            updateExpense(income);
                        }
                        else {

                            Expense income = new Expense();

                            income.setExpenseAmount(input_amount.getText().toString());
                            income.setExpensedate(date);
                            income.setExpenseMonth(selectedmonth);
                            income.setExpenseSource(input_source.getText().toString());
                            income.setExpenseyear(selectedYear);

                            addExpense(income);

                        }



                    }
                    else {

                        Toast.makeText(AddExpenseActivity.this,"Enter amount",Toast.LENGTH_SHORT).show();
                    }


                }
                else {

                    Toast.makeText(AddExpenseActivity.this,"Select date",Toast.LENGTH_SHORT).show();
                }




            }
        });

        if(expense_data!=null)
        {

            txtDate.setText(expense_data.getExpensedate());
            input_source.setText(expense_data.getExpenseSource());
            input_amount.setText(expense_data.getExpenseAmount());
            selectedmonth=expense_data.getExpenseMonth();
            selectedYear=expense_data.getExpenseyear();
            date=expense_data.getExpensedate();
        }


    }


    private void showDatePicker()
    {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddExpenseActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        date=dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        selectedmonth=(monthOfYear + 1)+"";
                        selectedYear=year+"";

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();



    }


    private void addExpense(final Expense income)
    {

        t=new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().expenseDao().insert(income);




            }
        });

        t.start();

        input_amount.setText("");
        date="";
        input_source.setText("");
        selectedmonth="";
        selectedYear="";
        txtDate.setText("");

    }

    private void updateExpense(final Expense income)
    {

        t=new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().expenseDao().update(income);




            }
        });

        t.start();

        input_amount.setText("");
        date="";
        input_source.setText("");
        selectedmonth="";
        selectedYear="";
        txtDate.setText("");
    }
}
