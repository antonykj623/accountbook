package com.centroid.accountbook.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.accountbook.R;
import com.centroid.accountbook.data.DatabaseClient;
import com.centroid.accountbook.domain.Income;

import java.util.Calendar;

public class AddIncomeActivity extends AppCompatActivity {

    ImageView imgback,imgdate;

    TextView txtDate;

    EditText input_amount,input_source;

    Button btn_submit;

    private int mYear, mMonth, mDay;

    String selectedmonth="",selectedYear="",date="";

    Thread t=null;
    Income income_data=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_income);
        getSupportActionBar().hide();

        income_data=(Income) getIntent().getSerializableExtra("Data");




        imgback=findViewById(R.id.imgback);
        imgdate=findViewById(R.id.imgdate);
        txtDate=findViewById(R.id.txtDate);
        input_amount=findViewById(R.id.input_amount);
        input_source=findViewById(R.id.input_source);
        btn_submit=findViewById(R.id.btn_submit);


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();

            }
        });

        imgdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDatePicker();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!date.equalsIgnoreCase(""))
                {
                    if(!input_amount.getText().toString().equalsIgnoreCase(""))
                    {


                        if(income_data!=null)
                        {

                            Income income = new Income();
                            income.setIncome_id(income_data.getIncome_id());
                            income.setIncomeAmount(input_amount.getText().toString());
                            income.setIncomedate(date);
                            income.setIncomeMonth(selectedmonth);
                            income.setIncomeSource(input_source.getText().toString());
                            income.setIncomeyear(selectedYear);
                            updateIncome(income);
                        }

                        else {


                            Income income = new Income();
                            income.setIncomeAmount(input_amount.getText().toString());
                            income.setIncomedate(date);
                            income.setIncomeMonth(selectedmonth);
                            income.setIncomeSource(input_source.getText().toString());
                            income.setIncomeyear(selectedYear);
                            addIncome(income);

                        }


                    }
                    else {

                        Toast.makeText(AddIncomeActivity.this,"Enter amount",Toast.LENGTH_SHORT).show();
                    }


                }
                else {

                    Toast.makeText(AddIncomeActivity.this,"Select date",Toast.LENGTH_SHORT).show();
                }




            }
        });

        if(income_data!=null)
        {

            txtDate.setText(income_data.getIncomedate());
            input_source.setText(income_data.getIncomeSource());
            input_amount.setText(income_data.getIncomeAmount());
            selectedmonth=income_data.getIncomeMonth();
            selectedYear=income_data.getIncomeyear();
            date=income_data.getIncomedate();
        }


    }


    private void updateIncome(final Income income)
    {

        t=new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().incomeDao().update(income);




            }
        });

        t.start();

        input_amount.setText("");
        date="";
        input_source.setText("");
        selectedmonth="";
        selectedYear="";
        txtDate.setText("");
    }


    private void addIncome(final Income income)
    {

        t=new Thread(new Runnable() {
            @Override
            public void run() {

                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().incomeDao().insert(income);




            }
        });

        t.start();

        input_amount.setText("");
        date="";
        input_source.setText("");
        selectedmonth="";
        selectedYear="";
        txtDate.setText("");

    }







    private void showDatePicker()
    {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(AddIncomeActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        date=dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        selectedmonth=(monthOfYear + 1)+"";
                        selectedYear=year+"";

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();



    }
}
