package com.centroid.accountbook.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.centroid.accountbook.R;
import com.centroid.accountbook.adapter.HomepagerAdapter;
import com.centroid.accountbook.fragments.ExpenseFragment;
import com.centroid.accountbook.fragments.IncomeFragment;
import com.centroid.accountbook.fragments.TotalFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    TabLayout tabLayout;

    ViewPager viewpager;

    IncomeFragment ifr=null;
    ExpenseFragment efr=null;
    TotalFragment tfr=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();

        viewpager=findViewById(R.id.viewpager);
        tabLayout=findViewById(R.id.tablayout);

        setupTabs();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if(ifr!=null)
        {
            ifr.getIncomeData();
        }

        if(efr!=null)
        {
            efr.getExpense();
        }

        if(tfr!=null)
        {
            tfr.getAllExpense();
        }
    }

    private void setupTabs()
    {




        List<Fragment>fragments=new ArrayList<>();


        for(int i=0;i<3;i++)
        {



            tabLayout.addTab(tabLayout.newTab());

            View v= LayoutInflater.from(this).inflate(R.layout.layout_customtabview,null);
            tabLayout.getTabAt(i).setCustomView(v);

            AppCompatImageView img=v.findViewById(R.id.imgtab);

            if(i==0)
            {

                ifr=new IncomeFragment();
                fragments.add(ifr);

                img.setImageResource(R.drawable.ic_money);

                img.setColorFilter(Color.parseColor("#ffffff"));

            }

            else if(i==1)
            {
                efr=new ExpenseFragment();
                fragments.add(efr);
                img.setImageResource(R.drawable.ic_expenses);
                img.setColorFilter(Color.parseColor("#7E7A7A"));

            }

            else if(i==2)
            {

                tfr=new TotalFragment();
                fragments.add(tfr);

                img.setImageResource(R.drawable.ic_description_);

                img.setColorFilter(Color.parseColor("#7E7A7A"));
            }
        }


        viewpager.setAdapter(new HomepagerAdapter(getSupportFragmentManager(),fragments));

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                tabLayout.getTabAt(position).select();





            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());


                View v= tab.getCustomView();


                AppCompatImageView img=v.findViewById(R.id.imgtab);






                img.setColorFilter(Color.parseColor("#ffffff"));



            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

                View v= tab.getCustomView();


                AppCompatImageView img=v.findViewById(R.id.imgtab);






                img.setColorFilter(Color.parseColor("#7E7A7A"));

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
