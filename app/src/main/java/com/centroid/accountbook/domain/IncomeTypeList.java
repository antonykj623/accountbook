package com.centroid.accountbook.domain;

import java.util.ArrayList;
import java.util.List;

public class IncomeTypeList {

    String monthyear;

    List<Income>incomes=new ArrayList<>();

    int selected=0;

    public IncomeTypeList() {
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getMonthyear() {
        return monthyear;
    }

    public void setMonthyear(String monthyear) {
        this.monthyear = monthyear;
    }

    public List<Income> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<Income> incomes) {
        this.incomes = incomes;
    }
}
