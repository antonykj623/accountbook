package com.centroid.accountbook.domain;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity
public class Income implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int Income_id;

    @ColumnInfo(name = "incomedate")
    private String incomedate;

    @ColumnInfo(name = "incomeMonth")
    private String incomeMonth;

    @ColumnInfo(name = "incomeyear")
    private String incomeyear;

    @ColumnInfo(name = "incomeAmount")
    private String incomeAmount;

    @ColumnInfo(name = "incomeSource")
    private String incomeSource;


    public Income() {
    }

    public String getIncomeSource() {
        return incomeSource;
    }

    public void setIncomeSource(String incomeSource) {
        this.incomeSource = incomeSource;
    }

    public int getIncome_id() {
        return Income_id;
    }

    public void setIncome_id(int income_id) {
        Income_id = income_id;
    }

    public String getIncomedate() {
        return incomedate;
    }

    public void setIncomedate(String incomedate) {
        this.incomedate = incomedate;
    }

    public String getIncomeMonth() {
        return incomeMonth;
    }

    public void setIncomeMonth(String incomeMonth) {
        this.incomeMonth = incomeMonth;
    }

    public String getIncomeyear() {
        return incomeyear;
    }

    public void setIncomeyear(String incomeyear) {
        this.incomeyear = incomeyear;
    }

    public String getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(String incomeAmount) {
        this.incomeAmount = incomeAmount;
    }
}
