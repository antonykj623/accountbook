package com.centroid.accountbook.domain;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Expense implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int expense_id;

    @ColumnInfo(name = "expensedate")
    private String expensedate;

    @ColumnInfo(name = "expenseMonth")
    private String expenseMonth;

    @ColumnInfo(name = "expenseyear")
    private String expenseyear;

    @ColumnInfo(name = "expenseAmount")
    private String expenseAmount;

    @ColumnInfo(name = "expenseSource")
    private String expenseSource;


    public Expense() {
    }

    public int getExpense_id() {
        return expense_id;
    }

    public void setExpense_id(int expense_id) {
        this.expense_id = expense_id;
    }

    public String getExpensedate() {
        return expensedate;
    }

    public void setExpensedate(String expensedate) {
        this.expensedate = expensedate;
    }

    public String getExpenseMonth() {
        return expenseMonth;
    }

    public void setExpenseMonth(String expenseMonth) {
        this.expenseMonth = expenseMonth;
    }

    public String getExpenseyear() {
        return expenseyear;
    }

    public void setExpenseyear(String expenseyear) {
        this.expenseyear = expenseyear;
    }

    public String getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(String expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getExpenseSource() {
        return expenseSource;
    }

    public void setExpenseSource(String expenseSource) {
        this.expenseSource = expenseSource;
    }
}
