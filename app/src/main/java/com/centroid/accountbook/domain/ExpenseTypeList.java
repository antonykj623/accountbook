package com.centroid.accountbook.domain;

import java.util.ArrayList;
import java.util.List;

public class ExpenseTypeList {

    String monthyear;

    List<Expense> expenses=new ArrayList<>();

    int selected=0;


    public ExpenseTypeList() {
    }


    public String getMonthyear() {
        return monthyear;
    }

    public void setMonthyear(String monthyear) {
        this.monthyear = monthyear;
    }

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }
}
